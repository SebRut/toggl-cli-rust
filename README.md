A simple cli for [Toggl](https://toggl.com/).

# Installation

Currently there are no prebuilt binaries available. To install run ```cargo install https://codeberg.org/SebRut/toggl-cli-rust.git``` or checkout the repository and run `cargo install` inside.

# Configuration

Create a config file at `~/.config/toggl-cli/config.toml` and add the following content:

```toml
api_key = "[YOUR TOGGL API KEY]"
```

# Usage

For an overview of available commands run `toggl-cli` without arguments.
