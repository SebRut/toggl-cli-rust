mod auth;
mod common;
mod time_entry;
mod workspaces;

use reqwest::{
    header::{HeaderMap, HeaderValue},
    Client, ClientBuilder, Url,
};
use secrecy::SecretString;
use thiserror::Error;

pub struct TogglApiClient {
    http_client: Client,
    base_url: Url,
    api_token: SecretString,
}

#[derive(Error, Debug)]
pub enum TogglApiClientError {
    #[error("parsing api base url failed")]
    UnsuccessfulBaseUrlParsing { source: url::ParseError },
    #[error("client creation failed")]
    UnsuccessfulHttpClientCreation,
    #[error("request failed")]
    FailedRequest { source: reqwest::Error },
    #[error("url creation failed")]
    FailedRequestUrlBuild { source: url::ParseError },
    #[error("response parsing failed")]
    FailedResponseParsing { source: reqwest::Error },
}

impl TogglApiClient {
    pub fn new_with_plain_api_token(
        api_token: String,
    ) -> Result<TogglApiClient, TogglApiClientError> {
        let safe_token: secrecy::Secret<String> = SecretString::new(api_token);
        Self::new(safe_token)
    }

    pub fn new(api_token: SecretString) -> Result<TogglApiClient, TogglApiClientError> {
        let base_url = Url::parse("https://api.track.toggl.com/api/")
            .map_err(|source| TogglApiClientError::UnsuccessfulBaseUrlParsing { source })?;

        let mut headers = HeaderMap::new();
        headers.insert("Content-Type", HeaderValue::from_static("application/json"));
        let http_client = ClientBuilder::new()
            .default_headers(headers)
            .build()
            .map_err(|_| TogglApiClientError::UnsuccessfulHttpClientCreation)?;

        Ok(Self {
            api_token,
            http_client,
            base_url,
        })
    }
}
