use reqwest::RequestBuilder;
use secrecy::ExposeSecret;

use super::TogglApiClient;
impl TogglApiClient {
    pub(crate) fn add_auth_to_request(&self, request_builder: RequestBuilder) -> RequestBuilder {
        request_builder.basic_auth(self.api_token.expose_secret(), Some("api_token"))
    }
}
