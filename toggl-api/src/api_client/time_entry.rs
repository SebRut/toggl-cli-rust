use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use super::{
    common::{TimeEntryId, WorkspaceId},
    TogglApiClient, TogglApiClientError,
};

impl TogglApiClient {
    pub async fn get_current_time_entry(&self) -> Result<Option<TimeEntry>, TogglApiClientError> {
        let url = self
            .base_url
            .join("v9/me/time_entries/current")
            .map_err(|source| TogglApiClientError::FailedRequestUrlBuild { source })?;

        let response = self
            .add_auth_to_request(self.http_client.get(url))
            .send()
            .await
            .map_err(|source| TogglApiClientError::FailedRequest { source })?;

        response
            .json()
            .await
            .map_err(|source| TogglApiClientError::FailedResponseParsing { source })
    }

    pub async fn stop_time_entry(
        &self,
        workspace_id: &WorkspaceId,
        time_entry_id: &TimeEntryId,
    ) -> Result<(), TogglApiClientError> {
        let url = self
            .base_url
            .join("v9/workspaces/")
            .and_then(|u| u.join(&format!("{}/", &workspace_id)))
            .and_then(|u| u.join("time_entries/"))
            .and_then(|u| u.join(&format!("{}/", &time_entry_id)))
            .and_then(|u| u.join("stop"))
            .map_err(|source| TogglApiClientError::FailedRequestUrlBuild { source })?;

        self.add_auth_to_request(self.http_client.patch(url))
            .send()
            .await
            .and_then(|r| r.error_for_status())
            .map_err(|source| TogglApiClientError::FailedRequest { source })?;

        Ok(())
    }

    pub async fn start_time_entry(
        &self,
        workspace_id: &WorkspaceId,
        description: String,
    ) -> Result<(), TogglApiClientError> {
        let url = self
            .base_url
            .join("v9/workspaces/")
            .and_then(|u| u.join(&format!("{}/", &workspace_id)))
            .and_then(|u| u.join("time_entries"))
            .map_err(|source| TogglApiClientError::FailedRequestUrlBuild { source })?;

        let body = StartTimeEntryBody {
            billable: None,
            created_with: "toggl-rust".to_owned(),
            description: Some(description),
            duration: -1,
            project_id: None,
            start: Utc::now(),
            stop: None,
            tag_ids: vec![],
            tags: vec![],
            task_id: None,
            user_id: None,
            workspace_id: *workspace_id,
        };

        self.add_auth_to_request(self.http_client.post(url))
            .json(&body)
            .send()
            .await
            .and_then(|r| r.error_for_status())
            .map_err(|source| TogglApiClientError::FailedRequest { source })?;

        Ok(())
    }
}

#[derive(Debug, Deserialize, PartialEq, Eq)]
pub struct TimeEntry {
    pub at: DateTime<Utc>,
    pub billable: bool,
    pub description: Option<String>,
    pub duration: i64,
    pub id: TimeEntryId,
    pub project_id: Option<i64>,
    pub server_deleted_at: Option<DateTime<Utc>>,
    pub start: DateTime<Utc>,
    pub stop: Option<DateTime<Utc>>,
    pub tag_ids: Option<Vec<i64>>,
    pub tags: Option<Vec<String>>,
    pub task_id: Option<i64>,
    pub user_id: i64,
    pub workspace_id: WorkspaceId,
}

#[derive(Debug, Serialize)]
struct StartTimeEntryBody {
    pub billable: Option<bool>,
    pub created_with: String,
    pub description: Option<String>,
    pub duration: i64,
    pub project_id: Option<i64>,
    pub start: DateTime<Utc>,
    pub stop: Option<DateTime<Utc>>,
    pub tag_ids: Vec<i64>,
    pub tags: Vec<String>,
    pub task_id: Option<i64>,
    pub user_id: Option<i64>,
    pub workspace_id: WorkspaceId,
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use reqwest::{Client, Url};
    use secrecy::SecretString;
    use wiremock::{
        matchers::{method, path},
        Mock, MockServer, ResponseTemplate,
    };

    use crate::api_client::TogglApiClient;

    #[tokio::test]
    async fn get_current_time_entry_fires_request_to_correct_endpoint_and_handles_response() {
        let mock_server = MockServer::start().await;
        let api_client = TogglApiClient {
            base_url: Url::from_str(&mock_server.uri()).unwrap(),
            api_token: SecretString::from_str("TOKEN").unwrap(),
            http_client: Client::new(),
        };

        Mock::given(path("/v9/me/time_entries/current"))
            .and(method("GET"))
            .respond_with(ResponseTemplate::new(200))
            .expect(1)
            .mount(&mock_server)
            .await;

        let _ = api_client.get_current_time_entry().await;
    }
}
