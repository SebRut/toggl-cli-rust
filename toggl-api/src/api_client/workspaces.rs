use serde::Deserialize;

use super::{common::WorkspaceId, TogglApiClient, TogglApiClientError};

impl TogglApiClient {
    pub async fn get_my_workspaces(&self) -> Result<Vec<Workspace>, TogglApiClientError> {
        let url = self
            .base_url
            .join("v9/me/workspaces")
            .map_err(|source| TogglApiClientError::FailedRequestUrlBuild { source })?;

        let response = self
            .add_auth_to_request(self.http_client.get(url))
            .send()
            .await
            .map_err(|source| TogglApiClientError::FailedRequest { source })?;

        response
            .json()
            .await
            .map_err(|source| TogglApiClientError::FailedResponseParsing { source })
    }
}

#[derive(Debug, Deserialize)]
pub struct Workspace {
    pub id: WorkspaceId,
}
