mod now;
mod start;
mod stop;

use clap::{Parser, Subcommand};
use toggl_api::api_client::TogglApiClient;

use self::{now::exec_now_command, start::exec_start_command, stop::exec_stop_command};

#[derive(Parser)]
struct Cli {
    #[command(subcommand)]
    pub commands: Commands,
}

#[derive(Subcommand)]
enum Commands {
    Now,
    Start { description: String },
    Stop,
}

pub async fn run_cli(client: TogglApiClient) -> color_eyre::Result<()> {
    let cli = Cli::try_parse()?;

    match cli.commands {
        Commands::Now => exec_now_command(client).await,
        Commands::Start { description } => exec_start_command(client, description).await,
        Commands::Stop => exec_stop_command(client).await,
    }
}
