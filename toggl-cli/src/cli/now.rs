use toggl_api::api_client::TogglApiClient;

pub async fn exec_now_command(client: TogglApiClient) -> color_eyre::Result<()> {
    let time_entry = client.get_current_time_entry().await?;

    match time_entry {
        Some(entry) => {
            println!(
                "description:\t{}",
                entry.description.unwrap_or("no description".to_string())
            );
            println!("start:\t\t{}", entry.start);
        }
        None => println!("No entry currently active."),
    }

    Ok(())
}
