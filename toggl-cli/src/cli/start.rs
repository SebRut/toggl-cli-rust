use color_eyre::eyre::{Context, ContextCompat};
use toggl_api::api_client::TogglApiClient;

pub async fn exec_start_command(
    client: TogglApiClient,
    description: String,
) -> color_eyre::Result<()> {
    let workspaces = client.get_my_workspaces().await?;
    let workspace_id = workspaces
        .first()
        .context("no context returned from api")?
        .id;
    client
        .start_time_entry(&workspace_id, description)
        .await
        .wrap_err("Failed to start new time entry")
}
