use toggl_api::api_client::TogglApiClient;

pub async fn exec_stop_command(client: TogglApiClient) -> color_eyre::Result<()> {
    let current_entry = client.get_current_time_entry().await?;

    match current_entry {
        Some(entry) => match client.stop_time_entry(&entry.workspace_id, &entry.id).await {
            Ok(_) => {
                println!(
                    "Entry \"{}\" stopped.",
                    entry.description.unwrap_or("".to_string())
                );
                Ok(())
            }
            Err(err) => {
                println!(
                    "An error occured while stopping entry \"{}\".",
                    entry.description.unwrap_or("".to_string())
                );
                Err(err.into())
            }
        },
        None => {
            println!("Currently no entry active.");
            Ok(())
        }
    }
}
