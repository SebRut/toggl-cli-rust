use color_eyre::eyre::ContextCompat;
use config::Config;
use directories::ProjectDirs;
use secrecy::SecretString;

const CONFIG_PREFIX: &str = "TOGGL_CLI";

pub(crate) fn get_config() -> color_eyre::Result<TogglCliConfig> {
    let config_file = ProjectDirs::from("de", "sebrut", "toggl-cli")
        .context("project dir creation failed")?
        .config_dir()
        .join("config");

    let config = Config::builder()
        .add_source(
            config::File::with_name(
                config_file
                    .to_str()
                    .context("config file path to str conversion failed")?,
            )
            .required(false),
        )
        .add_source(config::Environment::with_prefix(CONFIG_PREFIX))
        .build()?;

    let api_key = config.get_string("api_key").map(SecretString::new)?;

    let cli_config = TogglCliConfig { api_key };

    Ok(cli_config)
}

#[derive(Debug)]
pub(crate) struct TogglCliConfig {
    pub api_key: SecretString,
}
