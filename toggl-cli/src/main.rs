mod cli;
mod config;
use std::str::FromStr;

use crate::config::get_config;
use crate::config::TogglCliConfig;
use color_eyre::Result;
use toggl_api::api_client::TogglApiClient;
use tracing::Level;
use tracing_subscriber::{
    filter::Targets, prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt,
};

#[tokio::main]
async fn main() -> color_eyre::Result<()> {
    setup_tracing();
    color_eyre::install()?;

    let config = get_config()?;

    let client = setup_client(config)?;

    cli::run_cli(client).await
}

fn setup_tracing() {
    let filter = Targets::from_str(std::env::var("RUST_LOG").as_deref().unwrap_or("info"))
        .expect("RUST_LOG should be a valid tracing filter");
    tracing_subscriber::fmt()
        .with_max_level(Level::TRACE)
        .finish()
        .with(filter)
        .init();
}

fn setup_client(config: TogglCliConfig) -> Result<TogglApiClient> {
    Ok(TogglApiClient::new(config.api_key)?)
}
